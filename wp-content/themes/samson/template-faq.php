<?php
/*
 * Template Name: FAQ Template
 */
get_header(); ?>

<section class="samson-scoop samson-scoop-inner">
        <div class="site_container">
        <div class="inner_header">
            <h1>F<span class="border_red">AQs</span></h1>
        </div>
        <div class="samson-grid-tres">
            <div class="samson-feature feature-scoop">
                <div class="samson-feature-title no-pad">

<?php echo do_shortcode('[ultimate-faqs]'); ?>
                </div>
            </div>


        </div>
        </div>
</section>

<?php
get_footer();
