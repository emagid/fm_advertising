<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="home_splash" style="background-image:url(<?php the_field('splash'); ?>);">
    <div class="site_container">
        
        <div class="splash_content">
            <?php the_field('splash_text'); ?>
            <button class="btn_standard"><a href="#contact_scroll">Contact Us</a></button>
        </div>
    </div>
        
</section>

<section class="samson_stages stage_grey">
    <div class="site_container">
        <div class="stage_flex ">
        
        
            <div>
                <h2>Targeted Advertising</h2>
                <p>FM Advertising helps home owners find
local trusted contractors.</p>
            </div>

            <div>
                <h2>Creative <br>Experts</h2>
                <p>FM Advertising has an array of
experience in the contracting world which
helps you when it comes to finding the
right company for the job.</p>
            </div>

            <div>
                <h2>Competitive Pricing</h2>
                <p>FM Advertising offers some of the
cheapest advertising rates for local
contractors. </p>
            </div>
            
        </div>
    </div>
</section>

<section class="samson_stages ">
    <div class="site_container">
        <div class="words_candy_standard flex_reverse">
            <div class="words">
                <h2>Local Contractor Guide</h2>
                <p><?php the_field('samson_stages'); ?></p>
                
                <button class="btn_standard"><a href="/local-contractor-guide/">View More</a></button>
            </div>
            <div class="candy" >
                <img src="<?php the_field('image_one'); ?>">
                    
                
            </div>
        </div>
    </div>
</section>

<section class="samson_stages stage_black">
    <div class="site_container">
        <div class="words_candy_standard">
            <div class="candy candy_left" >
                <img src="<?php the_field('image_two'); ?>">
                    
                
            </div>
            <div class="words">
                <h2>Local Traveler Guide</h2>
                <p>This application is being placed in hotels all over the country. The goal of this is to list nearby attractions in the area so that users can browse and choose a restuarant or waterpark to make the best out of their trip.</p>
                
                <button class="btn_standard"><a href="/local-traveler-guide/">View More</a></button>
            </div>

        </div>
    </div>
</section>




<section class="contact_home" id="contact_scroll">
        <div class="site_container site_container_compact">
            <div class="inner_header">
                <p>Fill out the form and we’ll get right back to you.</p>
                <h2>Reach Out</h2>
            </div>


            <div class="contact_form">
                <?php echo do_shortcode('[contact-form-7 id="38" title="Quick Form"]'); ?>
            </div>
        </div>
</section>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
});

</script>


<?php
get_footer();
