<?php
/*
 * Template Name: Standard Template
 */

get_header(); ?>

<!--HOME - SPLASH-->



<section class="samson_stages stage_black">
    <div class="site_container">
        <div class="words_candy_standard non_flexed">
                <div class="words">
                <h2><?php the_title(); ?></h2>
                    <?php the_field('overview'); ?>
                
                
            </div>
            <div class="candy non_flexed" >
                <img src="<?php the_field('top_image'); ?>">
                    <button class="btn_standard"><a href="#contact_scroll">Contact Us</a></button>
                
            </div>


        </div>
    </div>
</section>




<section class="contact_home" >
        <div class="site_container site_container_compact">
            <div class="inner_header">
                <p>Fill out the form and we’ll get right back to you.</p>
                <h2>Reach Out</h2>
            </div>


            <div class="contact_form" id="contact_scroll">
                <?php echo do_shortcode('[contact-form-7 id="38" title="Quick Form"]'); ?>
            </div>
        </div>
</section>


<script>

    
$(function() {
    var header = $("#sticker");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 200) {
            header.addClass("darkHeader");
        } else {
            header.removeClass("darkHeader");
        }
    });
    
});
</script>
<?php
get_footer();
