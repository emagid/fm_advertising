<?php
get_header(); ?>

<!--HOME - SPLASH-->

<section class="inner">
    <div class="site_container">
        <div class="inner_header">
            <h1><span class="border_red">Ge</span>t Pricing</h1>
        </div>
        
        <div class="words_standard">
            <p><strong>We’re Ready For You! Let’s Start Something Awesome Together!</strong></p>
            <p>Samson Studios is your one stop location for all of your production needs in New York City.  We have everything you need whether its convenient locations, on site G&E rentals, studio space, modern amenities, and supportive, experienced, and enthusiastic staff.</p>  
            <p><strong>Come check us out and let’s get your project off the ground!</strong></p>
        </div>
        
        <div class="contact_form site_container_compact">
            <?php echo do_shortcode('[contact-form-7 id="24" title="Contact Us"]'); ?>
        </div>
    </div>
</section>
<?php
get_footer();
