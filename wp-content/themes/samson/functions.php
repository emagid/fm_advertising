<?php
/**
 * samson functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package samson
 */

if ( ! function_exists( 'samson_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function samson_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on samson, use a find and replace
		 * to change 'samson' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'samson', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'samson' ),
            'menu-2' => esc_html__( 'Footer', 'samson' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'samson_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'samson_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function samson_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'samson_content_width', 640 );
}
add_action( 'after_setup_theme', 'samson_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function samson_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Facebook', 'samson' ),
		'id'            => 'sidebar-1',
	) );
    register_sidebar( array(
		'name'          => esc_html__( 'Instagram', 'samson' ),
		'id'            => 'sidebar-2',
	) );
    register_sidebar( array(
		'name'          => esc_html__( 'Youtube', 'samson' ),
		'id'            => 'sidebar-3',
	) );
    register_sidebar( array(
		'name'          => esc_html__( 'Twitter', 'samson' ),
		'id'            => 'sidebar-4',
	) );
}
add_action( 'widgets_init', 'samson_widgets_init' );


/**
 * Increase File Upload Size
 */
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


/**
 * Enqueue scripts and styles.
 */
function samson_scripts() {
	wp_enqueue_style( 'samson-style', get_stylesheet_uri() );

	wp_enqueue_script( 'samson-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'samson-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'samson_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


//Here's my custom CSS that removes the back link in a function
function my_login_page_remove_back_to_link() { ?>
    <style type="text/css">
        body.login div#login p#backtoblog {
          display: none;
        }
        body.login {
            background:url(/wp-content/themes/samson/assets/img/samson-admin-login.jpg) !important;
            background-position: center;
        }
        .login h1 a {
            visibility: hidden !important;
        }
        .login form {
            background: rgba(255, 255, 255, 0.7) !important;
        }

        input[type=text]:focus, input[type=password]:focus {
            border-color: #E8A930 !important;
            box-shadow: 0 0 0 1px #E8A930 !important;
        }
        .wp-core-ui .button, .wp-core-ui .button-secondary {
            color: #E8A930 !important;
        }
                .wp-core-ui .button-primary {
            background: #E8A930 !important;
            border-color: #E8A930 !important;
            width: 100%;
            margin-top: 15px !important;
            color:#fff !important;
        }
    </style>


<?php }
//This loads the function above on the login page
add_action( 'login_enqueue_scripts', 'my_login_page_remove_back_to_link' );
