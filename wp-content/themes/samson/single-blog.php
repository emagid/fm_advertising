<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<section class="samson-scoop samson-scoop-inner">
        <div class="site_container">
        <div class="inner_header">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="samson-grid-tres">
            <div class="samson-feature feature-scoop">
                <div class="samson-feature-bg" style="background-image:url(<?php the_field('image'); ?>);"></div>
                <div class="samson-feature-title no-pad">
                    <div class="author-info">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/author.png">
                        <p><span>Written by <?php the_author(); ?></span><?php echo get_the_date(); ?></p>
                    </div>
                    <div class="blog_content">
                    <?php the_field('content'); ?></div>
                </div>
            </div>


        </div>
        </div>
</section>

<?php
get_footer();
