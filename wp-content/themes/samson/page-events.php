<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="page-events">
<section class="home_splash" style="background-image:url(<?php the_field('banner_image'); ?>)">
    <div class="site_container">
        
        <div class="splash_content">
            <h1><?php the_field('banner_title'); ?></h1>
        </div>
    </div>      
</section>

<section class="samson_stages">
    <div class="site_container">
            <div class="words">       
                <h2><span class="border_red_min">Abo</span>ut</h2>
                <?php the_field('about_text'); ?>
            </div>
        </div>
</section>



        <div class="specs-chart grey_bg">
            <div class="site_container">
                <div class="inner_header">
                <h2><span class="border_red_min">Fea</span>tures</h2></div>
                <ul>
                    <li>
                        <img src="<?php the_field('spec_icon_1'); ?>">
                        <p><?php the_field('spec_1'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_2'); ?>">
                        <p><?php the_field('spec_2'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_3'); ?>">
                        <p><?php the_field('spec_3'); ?></p>
                    </li>
                                        <li>
                        <img src="<?php the_field('spec_icon_4'); ?>">
                        <p><?php the_field('spec_4'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_5'); ?>">
                        <p><?php the_field('spec_5'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_6'); ?>">
                        <p><?php the_field('spec_6'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_7'); ?>">
                        <p><?php the_field('spec_7'); ?></p>
                    </li>
                    <li>
                        <img src="<?php the_field('spec_icon_8'); ?>">
                        <p><?php the_field('spec_8'); ?></p>
                    </li>
                    
                    <?php if (get_field('spec_9') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_9'); ?>">
                        <p><?php the_field('spec_9'); ?></p>
                    </li>
                        <?php endif; ?>   
                    
                    <?php if (get_field('spec_10') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_10'); ?>">
                        <p><?php the_field('spec_10'); ?></p>
                    </li>
                    <?php endif; ?> 
                    
                    <?php if (get_field('spec_11') != ''): ?>
                    <li>
                        <img src="<?php the_field('spec_icon_11'); ?>">
                        <p><?php the_field('spec_11'); ?></p>
                    </li>
                    <?php endif; ?> 
                </ul>
                <div class="cta_button">
                <button class="btn_standard"><a href="<?php the_field('pdf_link'); ?>" target="_blank">Download PDF</a></button></div>
            </div>
        </div>

<section class="slides">
    <div class="center">
        <?php echo do_shortcode('[visual_portfolio id="225" class=""]'); ?>
    </div>
</section>



<section class="contact_home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/samson-crew.jpg);">
        <div class="site_container site_container_compact">
            <div class="inner_header">
                <h2>Contact Us to Book Your Shoot</h2>
            </div>
            <div class="contact_form">
                <?php echo do_shortcode('[contact-form-7 id="38" title="Quick Form"]'); ?>
            </div>
        </div>
</section>
</section>

<?php
get_footer();
