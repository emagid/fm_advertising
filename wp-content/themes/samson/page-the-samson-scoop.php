<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

<!--HOME - SPLASH-->

<section class="samson-scoop samson-scoop-alt">
    <div class="default_container">
        <div class="site_container">
        <div class="inner_header">
            <h1><span class="border_red">Th</span>e Samson Scoop</h1>
        </div>
        
        
        <div class="samson-grid-tres">
            
                                                           <?php
	  			$args = array(
	    		'post_type' => 'blog'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
            
            
            
            <div class="samson-feature feature-scoop">
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <a href="<?php the_permalink(); ?>">
                    <div class="samson-feature-bg" style="background-image:url(<?php the_field('image'); ?>);"></div></a>
                <div class="samson-feature-title no-pad">
<!--                    <h6>Advisor</h6>-->
                    <div class="author-info">
<!--                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/author.png">-->
                        <p><span>Written by <?php the_author(); ?></span><?php echo get_the_date(); ?></p>
                    </div>
                    
                    
                    
                </div>
                            <div class="more_button">
                    <button class="btn_standard"><a href="<?php the_permalink(); ?>">Read More</a></button>
                </div>
            </div>
            
            
                                   <?php
			}
				}
			else {
			echo 'No Blogs Found';
			}
		?>    

        </div>
        </div>
    </div>
</section>


<?php
get_footer();
