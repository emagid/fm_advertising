<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package samson
 */

?>

<footer>
    <div class="site_container">
        <div class="samson-footer">
            <div class="samson-info">
                <a href="/">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/fm-logo.png"></a>
                <ul>
                    <li>
                    <p>11892 Flintwood St. NW Coon Rapids, MN 55448</p></li>
                    <li><p>(763) 350-9126 or (763) 688-1197</p></li>
                    
                </ul>
            </div>
        </div>
        <div class="samson-social">
            <li><p class="copy">Copyright © 2020 Fmadvertising - All Rights Reserved.</p></li>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<script>

    
$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});</script>

</body>
</html>
