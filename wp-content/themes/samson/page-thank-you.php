<?php
get_header(); ?>
<!-- Event snippet for General Thank You page conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-711519239/Ry9qCM6vwrgBEIfYo9MC'}); </script>

<!--HOME - SPLASH-->
<section class="inner">
    <div class="site_container">
        <div class="inner_header">
            <h1><span class="border_red">Th</span>ank You</h1>
        </div>
        
        <div class="words_standard">
            <p>Thank you for contacting us. We will be in touch with you as soon as possible.</p>
        </div>
    
    </div>
</section>

<?php
get_footer();
